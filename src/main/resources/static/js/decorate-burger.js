function resetAllSpinnerValue() {
    var collectionsOfSpinnerValues = document.getElementsByClassName("spinnerVal");
    for (i = 0; i < collectionsOfSpinnerValues.length; i++) {
        collectionsOfSpinnerValues[i].value = "0";
    }
}

$('[data-cardSelectButton]').click(function () {
    $(this).parent('[data-cardSelect]').toggleClass('is-selected');
});

$(document).ready(function () {
    $(document).on('click', '.selectBun', function () {
        if ($(this).text() === "Add to Cart") {
            $(this).text("Remove");
            $(this).css("background-color", "red");
            $(this).data('clicked', true);
            var desc = $(this).closest('.burger-card').find('#name').text();
            var cost = parseInt($(this).closest('.burger-card').find('#cost').text());
            var currentPrice = parseInt($('#priceTotal').text());
            currentPrice = currentPrice + cost;
            $('#priceTotal').text(currentPrice);
            $('#desc-burger').append(desc);
        } else {
            $(this).text("Add to Cart");
            $(this).css("background-color", "white");
            $(this).data('clicked', false);
            var desc = $(this).closest('.burger-card').find('#name').text();
            var cost = $(this).closest('.burger-card').find('#cost').text();
            var deselectBun = $('#selectBun').text().replace(desc, "");
            var currentPrice = parseInt($('#priceTotal').text());
            currentPrice = currentPrice - cost;
            $('#priceTotal').text(currentPrice);
            $('#desc-burger').text(deselectBun);
        }

    });

    $(document).on('click', '.selectFil', function () {
        if ($(this).text() === "+") {
            var desc = $(this).parent().closest('.burger-card').find('#name').text();
            var cost = parseInt($(this).parent().closest('.burger-card').find('#cost').text());
            var currentPrice = parseInt($('#priceTotal').text());
            var amount = parseInt($('#spinnerVal').text());
            currentPrice = currentPrice + cost;
            $('#priceTotal').text(currentPrice * amount);
            var textDesc = desc + " (" + amount + ") ";
            $('#desc-burger').append(textDesc);
        } else {
            var desc = $(this).parent().closest('.burger-card').find('#name').text();
            var cost = parseInt($(this).parent().closest('.burger-card').find('#cost').text());
            var deselectFil = $('#selectFil').text().replace(desc, "");
            var currentPrice = parseInt($('#priceTotal').text());
            var amount = parseInt($('#spinnerVal').text());
            currentPrice = currentPrice - cost;
            $('#priceTotal').text(currentPrice * amount);
            $('#desc-burger').text(deselectFil);
        }

    });

});