function colorizeStatus(status) {
    if (status == 'Order Placed') {
        $("#orderPlacedIcon").removeClass('Grayscale-Icon');
    } else if (status == "Prepared") {
        $("#orderPlacedIcon").removeClass('Grayscale-Icon');
        $("#preparedIcon").removeClass('Grayscale-Icon');
    } else if (status == "On The Way") {
        $("#orderPlacedIcon").removeClass('Grayscale-Icon');
        $("#preparedIcon").removeClass('Grayscale-Icon');
        $("#onTheWayIcon").removeClass('Grayscale-Icon');
    } else if (status == "Delivered") {
        $("#orderPlacedIcon").removeClass('Grayscale-Icon');
        $("#preparedIcon").removeClass('Grayscale-Icon');
        $("#onTheWayIcon").removeClass('Grayscale-Icon');
        $("#deliveredIcon").removeClass('Grayscale-Icon');
    }
}

$(document).ready(function(){
    if (status != "Cancelled") {
        $(".Cancelled-Status").hide();
        colorizeStatus(status);
    } else {
        $(".Delivery-Status").hide();
        $(".Cancel-Order").hide();
    }

    $('#cancelOrderButton').click(function() {
        $('#cancelOrderModal').modal('show');
    });

    $('#cancelConfirmButton').click(function() {
        var ids = $('#cancelConfirmButton').val();
        if (cancellable == "true") {
            $.ajax({
                url         : 'https://burgerqueen-order.herokuapp.com/cancelOrder/' + ids,
            })
            $('#cancelOrderModal').modal('hide');
            $(".template-modal-title-text").text("Success");
            $(".template-modal-body-text").text("You have successfully canceled this order!");
            $('#templateModal').modal('show');
            $(".Delivery-Status").hide();
            $(".Cancel-Order").hide();
            $(".Cancelled-Status").show();
        } else {
            alert("gabisa cancel");
        }
        });

    // setInterval(function () {
    //     var ids = $('#cancelConfirmButton').val();
    //     $.ajax({
    //         url         : 'https://burgerqueen-order.herokuapp.com/getOrderStatus/' + ids,
    //         success: function( data) {
    //             colorizeStatus(data);
    //             alert(data);
    //         }
    //     });
    // }, 3000);

    $('#uncancelConfirmButton').click(function() {
        $('#cancelOrderModal').modal('hide');
    });
});
