function resetAllSpinnerValue() {
    var collectionsOfSpinnerValues = document.getElementsByClassName("spinnerVal");
    var i;
    for (i = 0; i < collectionsOfSpinnerValues.length; i++) {
        collectionsOfSpinnerValues[i].value = "0";
    }
}

function getArrayOfValueOfAllBurgers() {
    var collectionsOfSpinnerValues = document.getElementsByClassName("spinnerVal");
    var arrayOfNames = [];
    var arrayOfValues = [];
    var arrayOfCost = [];
    var arrayOfIds = [];
    var arrayOfDescriptions = [];

    var i;
    for (i = 0; i < collectionsOfSpinnerValues.length; i++) {
        var divOfSpinnerVal = collectionsOfSpinnerValues[i].parentElement;
        var cardBody = divOfSpinnerVal.parentElement;
        var nameForSpinnerValue = cardBody.getElementsByClassName("card-title")[0].innerHTML; // Name of Card of Current Input
        var costForSpinnerValue = cardBody.querySelectorAll("h6.card-text")[0].innerHTML; // Cost of Card of Current Input
        var idForSpinnerValue = cardBody.getElementsByClassName("card-text")[0].innerHTML; // Id of Card of Current Input
        var descriptionForSpinnerValue = cardBody.getElementsByClassName("card-text")[1].innerHTML; // Description of Card of Current Input

        var spinnerValue = parseInt(collectionsOfSpinnerValues[i].value); // Value of current input
        if (arrayOfNames.includes(nameForSpinnerValue)) {
            var thatNameIndex = arrayOfNames.indexOf(nameForSpinnerValue);
            arrayOfValues[thatNameIndex] = arrayOfValues[thatNameIndex] + spinnerValue;
        } else {
            if (spinnerValue > 0) {
                arrayOfNames.push(nameForSpinnerValue);
                arrayOfValues.push(spinnerValue);
                arrayOfCost.push(costForSpinnerValue);
                arrayOfIds.push(idForSpinnerValue);
                arrayOfDescriptions.push(descriptionForSpinnerValue);
            }
        }
    }
    console.log(arrayOfNames);
    console.log(arrayOfValues);
    console.log(arrayOfCost);
    console.log(arrayOfIds);
    console.log(arrayOfDescriptions);
    return [arrayOfNames, arrayOfValues, arrayOfCost, arrayOfIds, arrayOfDescriptions];
}

function writeSummaryToModal() {
    resetSummaryModal();
    var arrayOfArrays = getArrayOfValueOfAllBurgers();
    var arrayOfNames = arrayOfArrays[0];
    var arrayOfValues = arrayOfArrays[1];
    var arrayOfCost = arrayOfArrays[2];

    var modalBody = document.getElementById("summaryModalBody");
    console.log(modalBody);

    if (arrayOfNames.length === 0) {
        modalBody.innerHTML = "You have not chosen any item.";
        var confirmButton = document.getElementById("confirmOrderButton");
        confirmButton.style.display = "none";

    } else {

        // Create Table
        var table = document.createElement("TABLE");
        table.className = "table table-borderless";

        // Create Heading
        var thead = document.createElement("THEAD");

        var tr = document.createElement("TR");
        var amount = document.createElement("TH");
        amount.innerHTML = "Amount";
        tr.appendChild(amount);

        var item = document.createElement("TH");
        item.innerHTML = "Item";
        tr.appendChild(item);

        var cost = document.createElement("TH");
        cost.innerHTML = "Cost";
        tr.appendChild(cost);

        thead.appendChild(tr);
        table.appendChild(thead);

        // Create body
        var tbody = document.createElement("TBODY");
        for (i = 0; i < arrayOfNames.length; i++) {
            tr = document.createElement("TR");
            var formValue = document.createElement("TD");
            formValue.innerHTML = arrayOfValues[i];
            tr.appendChild(formValue);

            var burgerName = document.createElement("TD");
            burgerName.innerHTML = arrayOfNames[i];
            tr.appendChild(burgerName);

            var burgerCostTotal = document.createElement("TD");
            burgerCostTotal.innerHTML = arrayOfCost[i] * arrayOfValues[i];
            tr.appendChild(burgerCostTotal);

            tbody.appendChild(tr);
        }
        table.appendChild(tbody);

        modalBody.appendChild(table);

    }

}

function resetSummaryModal() {
    var confirmButton = document.getElementById("confirmOrderButton");
    confirmButton.style.display = "initial";

    var modalBody = document.getElementById("summaryModalBody");
    var fc = modalBody.firstChild;

    while( fc ) {
        modalBody.removeChild( fc );
        fc = modalBody.firstChild;
    }
}

function confirmOrderButton() {
    var arrayOfArrays = getArrayOfValueOfAllBurgers();
    var arrayOfNames = arrayOfArrays[0];
    var arrayOfValues = arrayOfArrays[1];
    var arrayOfCost = arrayOfArrays[2];
    var arrayOfIds = arrayOfArrays[3];
    var arrayOfDescriptions = arrayOfArrays[4];

    var order = {"id_order": "", "burgers": []};
    var i;
    for (i = 0; i < arrayOfNames.length; i++) {
        var object = {};
        object["id"] = arrayOfIds[i];
        object["name"] = arrayOfNames[i];
        object["description"] = arrayOfDescriptions[i];
        object["cost"] = arrayOfCost[i];
        object["amount"] = arrayOfValues[i];
        order.burgers.push(object);
    }
    console.log(order);

    var jsonStringOfOrder = JSON.stringify(order);
    console.log(jsonStringOfOrder);

    $.ajax({
        type: "POST",
        contentType : 'application/json; charset=utf-8',
        dataType : 'json',
        url: "https://burgerqueen-order.herokuapp.com/order/make-order-from-json",
        data: jsonStringOfOrder,
        success :function(result) {
            console.log("POST Success");
            if (result == 1) {
                window.location.replace("https://burger-queen-web.herokuapp.com/order-your-burger");
            }
        }
    });
}


$('#summaryModal').on('shown.bs.modal', function () {
    $('#orderButton').trigger('focus')
})

$('#loginModal').modal('handleUpdate') // Dynamic height