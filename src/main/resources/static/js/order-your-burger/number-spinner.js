/** From: https://stackoverflow.com/questions/26586066/show-spinner-for-input-type-number-in-html5 */

$(document).ready(function() {

    /* alert("ready"); */
    var minusButton = $(".spinnerMinus"); //to aquire all minus buttons
    var plusButton = $(".spinnerPlus"); //to aquire all plus buttons

    //Handle click
    minusButton.click(function() {
        trigger_Spinner($(this), "-", {
            max: 99,
            min: 0
        }); //Triggers the Spinner Actuator
    }); /*end Handle Minus Button click*/

    plusButton.click(function() {
        trigger_Spinner($(this), "+", {
            max: 99,
            min: 0
        }); //Triggers the Spinner Actuator
    }); /*end Handle Plus Button Click*/

});



//This function will take the clicked button and actuate the spinner based on the provided function/operator
// - this allows you to adjust the limits of specific spinners based on classnames
function trigger_Spinner(clickedButton, plus_minus, limits) {

    var valueElement = clickedButton.closest('.customSpinner').find('.spinnerVal'); //gets the closest value element to this button
    var controllerbuttons = {
        minus: clickedButton.closest('.customSpinner').find('.spinnerMinus'),
        plus: clickedButton.closest('.customSpinner').find('.spinnerPlus')
    }; //to get the button pair associated only with this set of input controls

    //Activate Spinner
    updateSpinner(limits, plus_minus, valueElement, controllerbuttons); //to update the Spinner

}



/*
	max - maxValue
  min - minValue
  operator - +/-
  elem - the element that will be used to update the count
*/
function updateSpinner(limits, operator, elem, buttons) {

    var currentVal = parseInt(elem.val()); //get the current val

    //Operate on value -----------------
    if (operator == "+") {
        currentVal += 1; //Increment by one
        if (currentVal <= limits.max) {
            elem.val(currentVal);
        }
    } else if (operator == "-") {
        currentVal -= 1; //Decrement by one
        if (currentVal >= limits.min) {
            elem.val(currentVal);
        }
    }

    //Independent Controllers - Handle Buttons disable toggle ------------------------
    buttons.plus.prop('disabled', (currentVal >= limits.max)); //enable/disable button
    buttons.minus.prop('disabled', (currentVal <= limits.min)); //enable/disable button

}