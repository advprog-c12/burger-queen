package c12.burgerqueen;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

@Controller
public class StatusController {

    @GetMapping("/status/{id_akun}/{id_order}")
    public String status(@PathVariable long id_akun, @PathVariable long id_order, Map<String, Object> model) {
        String[] orderAttributes = getOrderStatus(id_akun, id_order);
        model.put("id_akun", orderAttributes[0]);
        model.put("id_order", orderAttributes[1]);
        model.put("status", orderAttributes[2]);
        model.put("cancellable", orderAttributes[3]);
        return "status";
    }

    public String[] getOrderStatus(long id_akun, long id_order) {
        String orderStatus = "";
        String orderId = "";
        String akunId = "";
        String cancellable = "";

        try {
            URL url = new URL("http://burgerqueen-order.herokuapp.com/getOrder/"
                + id_akun + "/" + id_order); // URL to Parse
            URLConnection urlConnection = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                JsonObject order = new Gson().fromJson(inputLine, JsonObject.class);
                akunId = order.get("id_akun").getAsString();
                orderId = order.get("id_order").getAsString();
                orderStatus = order.get("status").getAsString();
                cancellable = order.get("cancelable").getAsString();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] orderAttributes = new String[4];
        orderAttributes[0] = akunId;
        orderAttributes[1] = orderId;
        orderAttributes[2] = orderStatus;
        orderAttributes[3] = cancellable;
        return orderAttributes;
    }

    @GetMapping("/cancelOrder/{id_akun}/{id_order}")
    public void cancelOrder(long id_akun, long id_order) {
        try {
            URL url = new URL("http://burgerqueen-order.herokuapp.com/cancelOrder/"
                + id_akun + "/" + id_order); // URL to Parse
            url.openConnection();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
