package c12.burgerqueen;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OrderYourBurgerController {

    @CrossOrigin(origins = "*")
    @GetMapping("/order-your-burger")
    public String orderYourBurger(Model model) {

        JSONParser parser = new JSONParser();

        try {
            URL apiUrl = new URL("https://burger-queen-api.herokuapp.com/decorated");
            URLConnection apiUrlConnection = apiUrl.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(apiUrlConnection.getInputStream()));

            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                JSONArray jsonArray = (JSONArray) parser.parse(inputLine);
                System.out.println(jsonArray);
                model.addAttribute("burgers", jsonArray);

            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        model.addAttribute("title", "Order Your Burger");
        return "order-your-burger";
    }

}