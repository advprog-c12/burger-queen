package c12.burgerqueen;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Controller
public class DecorateBurgerController {

    @GetMapping("/decorate-burger")
    public String decorateBurger(Model model) {

        JSONParser parser = new JSONParser();

        try {
            URL apiUrlBuns = new URL("https://burger-queen-api.herokuapp.com/buns");
            URL apiUrlFillings = new URL("https://burger-queen-api.herokuapp.com/fillings");
            URLConnection apiUrlBunsConnection = apiUrlBuns.openConnection();
            URLConnection apiUrlFillingsConnection = apiUrlFillings.openConnection();
            BufferedReader readerBuns = new BufferedReader(new InputStreamReader(apiUrlBunsConnection.getInputStream()));
            BufferedReader readerFillings = new BufferedReader(new InputStreamReader(apiUrlFillingsConnection.getInputStream()));

            String inputLine;
            while ((inputLine = readerBuns.readLine()) != null) {
                JSONArray jsonArray = (JSONArray) parser.parse(inputLine);
                System.out.println(jsonArray);
                model.addAttribute("buns", jsonArray);
            }
            while ((inputLine = readerFillings.readLine()) != null) {
                JSONArray jsonArray = (JSONArray) parser.parse(inputLine);
                System.out.println(jsonArray);
                model.addAttribute("fillings", jsonArray);
            }
            readerBuns.close();
            readerFillings.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        model.addAttribute("title", "Decorate Your Burger");
        return "decorate-burger";
    }

}