package c12.burgerqueen;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Controller
public class AccountTestController {

        @GetMapping("/accounttest")
        public String account() {
            return "AccountTest";
        }

        @GetMapping("/orderhistory")
        public String status() {
            return "OrderHistory";
        }

        @GetMapping("/sign-in")
        public String signin() {
            return "Sign-in";
        }

        @GetMapping("/sign-out")
        public String signout() { return "sign-out"; }

        @GetMapping("/edit-profile")
        public String editprofile() { return "EditProfile"; }



    }


