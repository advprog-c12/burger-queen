package c12.burgerqueen;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = DecorateBurgerControllerTest.class)
public class DecorateBurgerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void decorateBurgerTitle() throws Exception {
        mockMvc.perform(get("/decorate-burger"))
                .andExpect(content().string(containsString("")));
    }

    @Test
    public void decorateYourBurgerTitleNegative() throws Exception {
        mockMvc.perform(get("/decorate-burger"))
                .andExpect(content().string(Matchers.not(containsString(
                        "https://burger-queen-web.herokuapp.com/decorate-burger"))));
    }

}
