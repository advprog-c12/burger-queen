package c12.burgerqueen;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = OrderYourBurgerController.class)
public class OrderYourBurgerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void orderYourBurgerTitle() throws Exception {
        mockMvc.perform(get("/order-your-burger"))
                .andExpect(content().string(containsString("Order Your Burger")));
    }

    @Test
    public void orderYourBurgerTitleNegative() throws Exception {
        mockMvc.perform(get("/order-your-burger"))
                .andExpect(content().string(Matchers.not(containsString(
                        "https://burger-queen-web.herokuapp.com/order-your-burger"))));
    }

}
