# Burger Queen

**Kelompok**: Bebek-Bebek Lucu Anonim (C-12) [![pipeline status](https://gitlab.com/advprog-c12/burger-queen/badges/master/pipeline.svg)](https://gitlab.com/advprog-c12/burger-queen/commits/master)[![coverage report](https://gitlab.com/advprog-c12/burger-queen/badges/master/coverage.svg)](https://gitlab.com/advprog-c12/burger-queen/commits/master)

**Anggota** :  
	- Abraham Williams Lumbantobing 	(1706039414)  
	- Julia Ningrum 					(1706979322)  
	- Kevin Christian Chandra 			(1706039976)  
	- Muhammad Anwar Farihin 			(1706039635)  
	- Novianti Aliasih 					(1506689111)  
 
**Deskripsi Aplikasi**  
Burger Queen adalah website yang menawarkan kustomisasi burger sesuai dengan minat customer. Burger Queen sendiri adalah tempat makan fast food baru buatan kami, Bebek-Bebek Lucu Anonim. Burger yang ditawarkan memiliki berbagai macam jenis roti, topping dan ukuran yang bisa dipilih oleh pelanggan.  

**Link Heroku**  
https://burger-queen-web.herokuapp.com/
